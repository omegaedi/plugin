package com.omegaedi.cordova.plugins.magtek;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.lang.Object;
import java.util.*;


import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CallbackContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.net.Uri;
import android.widget.Toast;

import com.omegaedi.cordova.plugins.magtek.MagtekReader;

import com.magtek.mobile.android.mtlib.MTEMVDeviceConstants;
import com.magtek.mobile.android.mtlib.MTSCRA;
import com.magtek.mobile.android.mtlib.MTConnectionType;
import com.magtek.mobile.android.mtlib.MTSCRAEvent;
import com.magtek.mobile.android.mtlib.MTEMVEvent;
import com.magtek.mobile.android.mtlib.MTConnectionState;
import com.magtek.mobile.android.mtlib.MTCardDataState;
import com.magtek.mobile.android.mtlib.MTDeviceConstants;
import com.magtek.mobile.android.mtlib.IMTCardData;
import com.magtek.mobile.android.mtlib.config.MTSCRAConfig;
import com.magtek.mobile.android.mtlib.config.ProcessMessageResponse;
import com.magtek.mobile.android.mtlib.config.SCRAConfigurationDeviceInfo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.ContextWrapper;
import android.view.ContextThemeWrapper;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.Handler.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;

public class Magtek extends CordovaPlugin {
	public String outMsg;

    public String mtKSN;
    public String mtDeviceSerial;

    //swipe readers
    public String mtSessionID;
    public String mtTrack1;
    public String mtTrack2;
    public String mtMPData;
    public String mtMPStatus;
    public String mtMaskedPAN;
    public String mtCardName;

	//emv readers
    public String mtEMVDataBlock;
    public String mtEncryptedBlock;
    public String mtDeviceModel;
    public String mtEMVEncoding;

    public static int TTCardReaderNone = 0;
	//we have other readers that aren't emv capable taking up slots 1-3
	public static int TTCardReaderEDynamo = 4;

	public static int TTCardEntryManual = 0;
	public static int TTCardEntrySwipe = 1;
	public static int TTCardEntryInsert = 2;

	private long m_ConnectStartTime=0;
	private MTConnectionType m_connectionType;
	private Handler mHandler;
	private String m_deviceName;
	private String m_deviceAddress;
	private String m_audioConfigType;
	private static final int REQUEST_ENABLE_BT = 3;

	private TextView mAddressField;
	private TextView mConnectionStateField;
	private MTConnectionState m_connectionState = MTConnectionState.Disconnected;
	public static final String PARTIAL_AUTH_INDICATOR = "1";

	private EditText mDataFields;
	// Intent request codes
	private static final int REQUEST_CONNECT_DEVICE = 1;

	private MTSCRA mMTSCRA;
	private UniversalCardReaderListener UCRL;
	private TLVParser TLVParser;
	//private int miDeviceType=MTSCRA.DEVICE_TYPE_NONE;
	private Handler mSCRADataHandler = new Handler(new SCRAHandlerCallback());
	String mStringLocalConfig;

	private int mIntCurrentDeviceStatus;
	private Set<BluetoothDevice> pairedDevices;

	private boolean mbAudioConnected;
	private ArrayList<String> list;

	private long mLongTimerInterval;
	private BluetoothAdapter mBluetoothAdapter;

	private int mIntCurrentStatus;
	private ListView lv;

	private int mIntCurrentVolume;
	private boolean mScanning;

	private Activity ParentActivity;
	private boolean reconnect;
	private HashMap<String, String> oldCardEventParams;

	private int CardEntryMethod;
	private static final String TAG = "MagTekPlug";

	private boolean fallbackEnabled;
	private int emvAttemptsBeforeFallbackSwipe;
	private MagtekReader magtekReaderFunctions = new MagtekReader();
	private String mStringAudioConfigResult;
	private CallbackContext mEventListenerCb;
	private int amount;


	private static final String MAGTEK = "MAGTEK-B3B1EB5";
	private static final long SCAN_PERIOD = 10000;

	public boolean getFallbackEnabled(){
		return fallbackEnabled;
	}

	public int getCardEntryMode(){
		return CardEntryMethod;
	}

	public HashMap<String, String> GetCardEventParams(int readerType){
	    HashMap<String, String> cardEventParams = new HashMap<>();
	    if(getCardEntryMode() == TTCardEntrySwipe){
	        if(readerType == TTCardReaderEDynamo) {
	            cardEventParams.put("TRANSACTION/CARDEVENTPARAMS/ENCDVCDEVICETYPE", "1");
	            cardEventParams.put("TRANSACTION/CARDEVENTPARAMS/MTKSN", magtekReaderFunctions.mtKSN);
	            cardEventParams.put("TRANSACTION/CARDEVENTPARAMS/MTSESSIONID", magtekReaderFunctions.mtSessionID);
	            cardEventParams.put("TRANSACTION/CARDEVENTPARAMS/MTENCRYPTEDTRACK1", magtekReaderFunctions.mtTrack1);
	            cardEventParams.put("TRANSACTION/CARDEVENTPARAMS/MTENCRYPTEDTRACK2", magtekReaderFunctions.mtTrack2);
	            cardEventParams.put("TRANSACTION/CARDEVENTPARAMS/MTDEVICESERIAL", magtekReaderFunctions.mtDeviceSerial);
	            cardEventParams.put("TRANSACTION/CARDEVENTPARAMS/MTMPDATA", magtekReaderFunctions.mtMPData);
	            cardEventParams.put("TRANSACTION/CARDEVENTPARAMS/MTMPSTATUS", magtekReaderFunctions.mtMPStatus);
	        }
	    } else if(getCardEntryMode() == TTCardEntryInsert){
	        if(readerType == TTCardReaderEDynamo) {
	            cardEventParams.put("TRANSACTION/CARDEVENTPARAMS/MTENCRYPTEDBLOCK", magtekReaderFunctions.mtEncryptedBlock);
	            cardEventParams.put("TRANSACTION/CARDEVENTPARAMS/MTKSN", magtekReaderFunctions.mtKSN);
	            cardEventParams.put("TRANSACTION/CARDEVENTPARAMS/MTDEVICESERIAL", magtekReaderFunctions.mtDeviceSerial);
	            cardEventParams.put("TRANSACTION/CARDEVENTPARAMS/ENCDVCDEVMODEL", magtekReaderFunctions.mtDeviceModel);
	            cardEventParams.put("TRANSACTION/EMVDATA/EMVDATABLOCK", magtekReaderFunctions.mtEMVDataBlock);
	            cardEventParams.put("TRANSACTION/EMVDATA/EMVENCODING", magtekReaderFunctions.mtEMVEncoding);
	            cardEventParams.put("TRANSACTION/QUICKCHIP", "TRUE");
	        }
	    }
	    Log.i(TAG, "cardEntryMode is: " + CardEntryMethod);
	    //sendJavascript("cardEntryMode is: " + CardEntryMethod);
	    return cardEventParams;
	}

	private void sendJavascript(final String message) {
		webView.getView().post(new Runnable() {
	        public void run() {
		            // Finally do whatever you want with 'appView', for example:
		            if(message != null){
		        	String newMessage = message.replace("\"", "\\\"");
		        	newMessage = newMessage.replace("\'", "\\'");
		            //webView.loadUrl("javascript:" + "Rollbar.debug(\"From Android:\")");
		            webView.loadUrl("javascript:" + "console.log(\'From Android: "+newMessage+"\')");
		            webView.loadUrl("javascript:" + "Rollbar.debug(\'From Android: "+newMessage+"\')");
		        }
	        }
	    });
	}

	private final BroadcastReceiver mReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
				String action = intent.getAction();

				// When discovery finds a device
				if (BluetoothDevice.ACTION_FOUND.equals(action))
				{
						// Get the BluetoothDevice object from the Intent
						BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
						// If it's already paired, skip it, because it's been listed already
						if (device.getBondState() != BluetoothDevice.BOND_BONDED)
						{

						}
				// When discovery is finished, change the Activity title
				}
				else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action))
				{
						mScanning = false;
				}
		}
	};

	private void InitializeDevice() {
	 	boolean connected = false;
	 	mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	    reconnect = false;
	    oldCardEventParams = new HashMap<>();
	    fallbackEnabled = false;
	    CardEntryMethod = TTCardEntrySwipe;
	    ParentActivity = cordova.getActivity();
	    SharedPreferences settings = ParentActivity.getSharedPreferences("com.omegaedi.magtek.MyPrefsFile", 0);

	    emvAttemptsBeforeFallbackSwipe = settings.getInt("emvAttemptsBeforeFallback", 3);
	    // updateText("No reader found","red");
	    HashMap<String, String> startResult = new HashMap<>();
	    startResult.put("RESULT","Success"); //we'll override this later if we fail
	    int readerType = settings.getInt("ReaderType", TTCardReaderNone);
	    // toast("ReaderType: " + readerType);
	    // if (readerType == TTCardReaderEDynamo) { //MagTek BulleT (Bluetooth) and Magtek eDynamo
	        
	        if (mBluetoothAdapter != null) {
	        	if (!mBluetoothAdapter.isEnabled()){
	        		Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		    		cordova.getActivity().startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
	        	}
	            // pairedDevices = mBluetoothAdapter.getBondedDevices();
	            // if (pairedDevices.size() > 0 && settings.contains("ReaderAddress")) {
	            //     for (BluetoothDevice device : pairedDevices) {
	            //         String saved = settings.getString("ReaderAddress", "");
	            //         String thisDevice = device.getAddress();
	            //         if (thisDevice.equals(saved)) {
	            //             if (mMTSCRA != null && mMTSCRA.isDeviceConnected()){
	            //                 continue;
	            //             }
	            //             toast("Into this section");
	            //             magtekReaderFunctions.outMsg = "Connecting to reader...";
	            //             updateText(magtekReaderFunctions.outMsg,"black");
	                        mMTSCRA = new MTSCRA(ParentActivity.getApplicationContext(), mSCRADataHandler);
	                        mMTSCRA.clearBuffers();
	                        // if (readerType == TTCardReaderEDynamo) { //eDynamo
	                            m_connectionType = MTConnectionType.BLEEMV;
	                            mMTSCRA.setConnectionType(m_connectionType);
	                            // mMTSCRA.setConnectionRetry(true);
	                        // } else {
	                        //     m_connectionType = MTConnectionType.Bluetooth;
	                        //     mMTSCRA.setConnectionType(m_connectionType);
	                        // }
	                        // mMTSCRA.setAddress(device.getAddress());
	                        // openMagtekDevice();
	                        // connected = true;
	                        // break; //device found, break out of for loop
	                   // }
	                //}
	                // if (!connected && !settings.getBoolean("DontShow", false)) { //configured device not found
	                //     startResult.put("RESULT","BluetoothReaderNotFound");
	                // }
	            // } else { //no paired devices found
	            //     Boolean b = settings.getBoolean("DontShow", false);
	            //     if (!b) {
	            //         startResult.put("RESULT","NoPairedDevice");
	            //     }
	            // }
	        } else { //bluetooth not enabled
	            Boolean b = settings.getBoolean("DontShow", false);
	            if (!b) {
	                startResult.put("RESULT","BluetoothCurrentlyDisabled");
	            }
	        }
	    // } else {
	    //     updateText("No card reader configured","red");
	    //     updateText(startResult.toString(),"beef");
    	// }
    	// toast(startResult.toString());
    }

    private void updateText(String text, String color){
	    HashMap<String, String> updatedText = new HashMap<>();
	    updatedText.put("TEXT",text);
	    updatedText.put("COLOR", color);
	    toast(text);
	}	

	void toast(String str){
	Toast.makeText(
		webView.getContext(),
		str,
		Toast.LENGTH_LONG
	).show();
}

	// CardTransaction cardTran = new CardTransaction();
	// cardTran.AddRequestVariable("TRANSACTION/SERVICENAME", card);
	// cardTran.AddRequestVariable(“TRANSACTION/RNID”, rnid);
	// cardTran.AddRequestVariable(“TRANSACTION/RNCERT”, rncert);

	// cardTran.AddRequestVariable("TRANSACTION/CCAMT", gTotalAmount);

	// cardTran.AddRequestVariable("TRANSACTION/CUSTIDENT", custid.getText().toString());

	// cardTran.AddRequestVariable("TRANSACTION/TRANIDENTGUID", TRANIDENTGUID);
	// if(STATIONIDENT != null && !STATIONIDENT.isEmpty()) {
	//     cardTran.AddRequestVariable("TRANSACTION/STATIONIDENT", STATIONIDENT);
	// }

	// cardTran.AddRequestVariable("TRANSACTION/TRANIDENT", tranId);
	        
	// if(tranType == TRAN_TYPE_CCCREDIT){
	//     cardTran.AddRequestVariable("TRANSACTION/TRANSACTIONTYPE", "CCCREDIT");
	// } else {
	//     cardTran.AddRequestVariable("TRANSACTION/TRANSACTIONTYPE", "CCAUTH");
	//     //you can only accept a partial auth on a ccauth so check inside the if statement
	//     if(settings.getBoolean("SNAcceptPartialAuth" + card, false)){
	//         cardTran.AddRequestVariable("TRANSACTION/ACCEPTPARTIALAUTH", "TRUE");
	//     }
	// }

	// for (HashMap.Entry<String, String> entry : cardEventParams.entrySet()) {
	//     cardTran.AddRequestVariable(entry.getKey(), entry.getValue());
	// }

	@Override
	public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
		PluginResult pr = new PluginResult(PluginResult.Status.ERROR, "Unhandled execute call: " + action);	


		if(action.equals("openDevice")) {
			if(mMTSCRA == null) {
				InitializeDevice();
			}

				if (!mMTSCRA.isDeviceConnected()){
					mMTSCRA.setConnectionType(m_connectionType);
					// mMTSCRA.setConnectionRetry(true);
					mMTSCRA.setAddress(m_deviceAddress);
					//toast("Trying to connect at " + m_deviceAddress);
	      			mMTSCRA.openDevice();
					m_deviceName = mMTSCRA.getDeviceName();
				}
        pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.isDeviceConnected());

		}
		else if(action.equals("defaultDevice")) {

			if(mMTSCRA == null) {
				InitializeDevice();
			}

			if (!mBluetoothAdapter.isEnabled()) {
			    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			    cordova.getActivity().startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
			}
		  	else {
				pairedDevices = mBluetoothAdapter.getBondedDevices();

		      	for(BluetoothDevice bt : pairedDevices){
		         	String devName = bt.getName();
					String devAddress = bt.getAddress();
					if (devName.equals(MAGTEK)){
					 	m_deviceAddress = devAddress;
					}
				}
			}
				mMTSCRA.setConnectionType(m_connectionType);
				// mMTSCRA.setConnectionRetry(true);
				mMTSCRA.setAddress(m_deviceAddress);
				mMTSCRA.openDevice();
				//toast("Connected to: " + mMTSCRA.getDeviceName());


				pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.isDeviceConnected());
			}

		else if (action.equals("chooseDevice")) {
			if(mMTSCRA == null) {
				InitializeDevice();
			}
			m_deviceAddress = args.getString(0);
			SharedPreferences settings = ParentActivity.getSharedPreferences("com.omegaedi.magtek.MyPrefsFile", 0);
			String saved = settings.getString("ReaderAddress", "");
			
			//toast("Opening Device at " + m_deviceAddress);

			if(saved == m_deviceAddress){
				Log.i(TAG, "Device address was saved in: " + m_deviceAddress);
				sendJavascript("Device address was saved in: " + m_deviceAddress);
				mMTSCRA.setConnectionRetry(true);
			}
			mMTSCRA.setAddress(m_deviceAddress);

			// toast("Trying at " + m_deviceAddress);
			mMTSCRA.openDevice();



			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getBatteryLevel());
		}

		else if (action.equals("getPaired")) {
			// toast("Trying to get paired");
			if(mMTSCRA == null) {
				InitializeDevice();
			}
			pairedDevices = mBluetoothAdapter.getBondedDevices();
			if(pairedDevices.size() == 0){
				//toast("It's empty maang");
			}
			JSONArray bts = new JSONArray();
			for (BluetoothDevice bt : pairedDevices){
				JSONObject pbtd = new JSONObject();
				pbtd.put("label", bt.getName());
				pbtd.put("value", bt.getAddress());
				bts.put(pbtd);

			}

			pr = new PluginResult(PluginResult.Status.OK, bts);
		}

		else if(action.equals("getSavedDevice")) {
			ParentActivity = cordova.getActivity();
			SharedPreferences settings = ParentActivity.getSharedPreferences("com.omegaedi.magtek.MyPrefsFile", 0);
			String saved = settings.getString("ReaderAddress", "");

			if(saved == ""){
				saved = "No Address";
			}

			pr = new PluginResult(PluginResult.Status.OK, saved);
		}

		else if(action.equals("getBatteryLevel")) {
			if(mMTSCRA == null) {
				InitializeDevice();
			}

			Log.i(TAG, "Supposed to grab me some battery");
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getBatteryLevel());
		}
		
		else if(action.equals("closeDevice")) {
			mMTSCRA.closeDevice();

			pr = new PluginResult(PluginResult.Status.OK, !mMTSCRA.isDeviceConnected());
		}
		else if(action.equals("isDeviceConnected")) {
			Log.i(TAG, "Sending this result: " + mMTSCRA.isDeviceConnected());
			//sendJavascript("isConnected result: " + mMTSCRA.isDeviceConnected());
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.isDeviceConnected());
		}
		else if(action.equals("isDeviceOpened")) {
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.isDeviceConnected());
		}
		else if(action.equals("clearCardData")) {
			pr = new PluginResult(PluginResult.Status.OK);
		}
/*		else if(action.equals("setCardData")) {
			try {
				;
			}
		}
		else if(action.equals("getTrackDecodeStatus")) {
			try {
				;
			}
		}
*/
		else if(action.equals("getTrack1")) {
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getTrack1());
		}
		else if(action.equals("getTrack2")) {
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getTrack2());
		}
		else if(action.equals("getTrack3")) {
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getTrack3());
		}
		else if(action.equals("getTrack1Masked")) {
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getTrack1Masked());
		}
		else if(action.equals("getTrack2Masked")) {
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getTrack2Masked());
		}
		else if(action.equals("getTrack3Masked")) {
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getTrack3Masked());
		}
		else if(action.equals("getMagnePrintStatus")) {
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getMagnePrintStatus());
		}
		else if(action.equals("getMagnePrint")) {
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getMagnePrint());
		}
		else if(action.equals("getDeviceSerial")) {
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getDeviceSerial());
		}
		else if(action.equals("getSessionID")) {
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getSessionID());
		}
		else if(action.equals("setSavedDevice")) {
			ParentActivity = cordova.getActivity();
			m_deviceAddress = args.getString(0);
			SharedPreferences settings = ParentActivity.getSharedPreferences("com.omegaedi.magtek.MyPrefsFile", 0);
			Log.i(TAG, "Trying to set them prefences");
			sendJavascript("Trying to set them prefences");
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("ReaderAddress", m_deviceAddress);
			editor.commit();

			pr = new PluginResult(PluginResult.Status.OK, settings.getString("ReaderAddress", ""));
		}
/*		else if(action.equals("setDeviceProtocolString")) {
			try {
				;
			}
		}
*/
		else if(action.equals("listenForCardEvents")) {
			Log.i(TAG, "Listening for Card Events");
			pr = new PluginResult(PluginResult.Status.NO_RESULT);
			pr.setKeepCallback(true);


			this.mEventListenerCb = callbackContext;
			startEDynamoEMVTransactionFlow();
		}
		else if(action.equals("listenForEvents")) {
			Log.i(TAG, "Listening for Connection Events");
			pr = new PluginResult(PluginResult.Status.NO_RESULT);
			pr.setKeepCallback(true);


			this.mEventListenerCb = callbackContext;
		}
		else if(action.equals("getCardName")) {
			//toast("Getting name..");
			sendToDisplay(mMTSCRA.getCardName());
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getCardName());
		}
		else if(action.equals("getCardIIN")) {
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getCardIIN());
		}
		else if(action.equals("getCardLast4")) {
			//toast("Getting Last4");
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getCardLast4());
		}
		else if(action.equals("getCardExpDate")) {
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getCardExpDate());
		}
		else if(action.equals("getCardServiceCode")) {
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getCardServiceCode());
		}
		else if(action.equals("getCardStatus")) {
			pr = new PluginResult(PluginResult.Status.OK, mMTSCRA.getCardStatus());
		}
		else if(action.equals("setAmount")) {
			Double amount = Double.parseDouble(args.getString(0));
			amount = amount * 100;
			this.amount = amount.intValue();
			pr = new PluginResult(PluginResult.Status.OK, this.amount);
		}
/*
		else if(action.equals("setDeviceType")) {
			try {
				;
			}
		}
*/
		else if(action.equals("setDeviceType")) {
			;
		}

		else if(action.equals("getDevAddress")) {
			pr = new PluginResult(PluginResult.Status.OK, m_deviceAddress);
		}

		
		callbackContext.sendPluginResult(pr);


		return true;
	}

    @Override
    public void onResume(boolean multitasking) {
    	super.onResume(multitasking);



        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity
        // returns.
    }

    @Override
    public void onDestroy() {

        if (mMTSCRA != null)
            mMTSCRA.closeDevice();

    	super.onDestroy();
    }

    protected void OnDeviceStateChanged(MTConnectionState deviceState)
    {
    	MTConnectionState tPrevState =m_connectionState;

    	setState(deviceState);
    	m_deviceName = mMTSCRA.getDeviceName();

    	JSONObject response = new JSONObject();
    	
    	
    	switch (deviceState)
    	{
    		case Disconnected:
    	        Log.i(TAG, "OnDeviceStateChanged=Disconnected");
    	        sendJavascript("OnDeviceStateChanged=Disconnected");
							// toast("Disconnected");
							       try{
    	        	response.put("State", "Disconnected");
    	        }
    	        catch(JSONException e) {}
    	        this.mEventListenerCb.success(response);
    			break;
			case Connected:
    	        Log.i(TAG, "OnDeviceStateChanged=Connected");
    	        sendJavascript("OnDeviceStateChanged=Connected");
				toast("Connected to: " + m_deviceName);
				try{
				response.put("State", "Connected");
				response.put("Battery", mMTSCRA.getBatteryLevel());
				}
				catch(JSONException e) {}	
				this.mEventListenerCb.success(response);
				break;
    		case Error:
    	        Log.i(TAG, "OnDeviceStateChanged=Error");
    	        sendJavascript("OnDeviceStateChanged=Error");
					try{
	    			response.put("State", "Error");
					}
					catch(JSONException e)
					{}
					this.mEventListenerCb.error(response);
    			break;
			case Connecting:
    	        Log.i(TAG, "OnDeviceStateChanged=Connecting");
    	        toast("Connecting");
    	        sendJavascript("OnDeviceStateChanged=Connecting");
				break;
			case Disconnecting:
    	        Log.i(TAG, "OnDeviceStateChanged=Disconnecting");
    	        sendJavascript("OnDeviceStateChanged=Disconnecting");
    	 
				break;
    	}
    	
    }

    private void startEDynamoEMVTransactionFlow() {
	    if (mMTSCRA != null) {
	        byte timeLimit = 0x3C;
	        byte cardType = 0x03;  // MSR + Chip
	        byte option = 0x00;
	        String paddedAmount = String.format("%012d", this.amount);
	        //byte[] amount = TLVParser.getByteArrayFromHexString(paddedAmount);
	        byte[] amount = new byte[]{0x00, 0x00, 0x00, 0x00, 0x01, 0x00};
	        byte transactionType = 0x00;
	        byte[] cashBack = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	        byte[] currencyCode = new byte[]{0x08, 0x40};
	        byte reportingOption = 0x02;


	        int transaction = mMTSCRA.startTransaction(timeLimit, cardType, option, amount, transactionType, cashBack, currencyCode, reportingOption);
	    	// toast("Please Use Chip Reader");
	    	Log.i(TAG, "startTransaction result code: " + transaction);
	    	sendJavascript("startTransaction result code: " + transaction);
	    }
	}

	private void sendCardData() throws JSONException {
		JSONObject response = new JSONObject();

		response.put("State", "Swiped");
		response.put("Response.Type", mMTSCRA.getResponseType());
		response.put("Track.Status", mMTSCRA.getTrackDecodeStatus());
		response.put("Card.Status", mMTSCRA.getCardStatus());
		response.put("Card.Pan", mMTSCRA.getCardPAN());
		response.put("Encryption.Status", mMTSCRA.getEncryptionStatus());
		response.put("Battery.Level", mMTSCRA.getBatteryLevel());
		response.put("Swipe.Count", mMTSCRA.getSwipeCount());
		response.put("Track.Masked", mMTSCRA.getMaskedTracks());
		response.put("MagnePrint.Status", mMTSCRA.getMagnePrintStatus());
		response.put("SessionID", mMTSCRA.getSessionID());
		response.put("Card.SvcCode", mMTSCRA.getCardServiceCode());
		response.put("Card.PANLength", mMTSCRA.getCardPANLength());
		response.put("KSN", mMTSCRA.getKSN());
		response.put("Device.SerialNumber", mMTSCRA.getDeviceSerial());
		response.put("TLV.CARDIIN", mMTSCRA.getTagValue("TLV_CARDIIN: ", ""));
		response.put("MagTekSN", mMTSCRA.getMagTekDeviceSerial());
		response.put("FirmPartNumber", mMTSCRA.getFirmware());
		response.put("TLV.Version", mMTSCRA.getTLVVersion());
		response.put("DevModelName", mMTSCRA.getDeviceName());
		response.put("MSR.Capability", mMTSCRA.getCapMSR());
		response.put("Tracks.Capability", mMTSCRA.getCapTracks());
		response.put("Encryption.Capability", mMTSCRA.getCapMagStripeEncryption());
		response.put("Card.IIN", mMTSCRA.getCardIIN());
		response.put("Card.Name", mMTSCRA.getCardName());
		response.put("Card.Last4", mMTSCRA.getCardLast4());
		response.put("Card.ExpDate", mMTSCRA.getCardExpDate());
		response.put("Track1.Masked", mMTSCRA.getTrack1Masked());
		response.put("Track2.Masked", mMTSCRA.getTrack2Masked());
		response.put("Track3.Masked", mMTSCRA.getTrack3Masked());
		response.put("Track1", mMTSCRA.getTrack1());
		response.put("Track2", mMTSCRA.getTrack2());
		response.put("Track3", mMTSCRA.getTrack3());
		response.put("MagnePrint", mMTSCRA.getMagnePrint());
		response.put("RawResponse", mMTSCRA.getResponseData());
		//PluginResult pr = new PluginResult(PluginResult.Status.OK, response);
		
		
		this.mEventListenerCb.success(response);
		//toast("Received Card Info");
		//pr.setKeepCallback(false);
		// toast(response.toString());
	}

	private void sendTransactionResult(int readerType) throws JSONException {
		HashMap<String, String> cardEventParams = GetCardEventParams(readerType);
		JSONObject result = new JSONObject(cardEventParams);
		String stringifiedJSON = result.toString();
		String entryMode = getCardEntryMode() == 2 ? "EMV" : "Swipe";

		result.put("CardType", entryMode);
		result.put("Stringified", stringifiedJSON);
		result.put("Battery", mMTSCRA.getBatteryLevel());
		String mtskn = result.getString("TRANSACTION/CARDEVENTPARAMS/MTKSN");
		Log.i(TAG, "MTSKN value comparing: " + mtskn);

		if(!mtskn.equals("") && !mtskn.equals(null) && !mtskn.equals("null") && !oldCardEventParams.equals(cardEventParams)){
			this.mEventListenerCb.success(result);
			sendJavascript("Sent Card Data Back");
			Log.i(TAG, "Supposed to have sent:" + result);
			magtekReaderFunctions.ClearMagtekData();
			
		}
		else{
			sendJavascript("Didn't send data, null or matched old data");
		}
		//sendJavascript("OldCardParams: " + oldCardEventParams);
		oldCardEventParams = cardEventParams;
		//mMTSCRA.clearBuffers();
		//sendJavascript(result.toString());
		// sendJavascript("Mama's green casserole");
		// //sendJavascript("Mama\\'s green beans");
		// sendJavascript("Mama\"s green eggs");
		// //magtekReaderFunctions.ClearMagtekData();

		
		// toast("Here's that result ma'am");
	}

	private void sendError(String error) {
		this.mEventListenerCb.error(error);
	}

	private void sendEMVParams(String paddedAmount, byte[] byteAmount) throws JSONException {
		JSONObject response = new JSONObject();
        response.put("paddedAmount", paddedAmount);
        response.put("byteAmount", amount);
        this.mEventListenerCb.success(response);
	}

	private void sendToDisplay(final String data) throws JSONException
			{
				JSONObject response = new JSONObject();
					if (data != null)
					{
						//response.put("Data: " + data);						
						toast(data.toString());
					}
			}

	private void sendCardError() {
		toast("That card was not swiped properly. Please try again.");
	}

	private class SCRAHandlerCallback implements Handler.Callback {
    public boolean handleMessage(Message msg) {
        SharedPreferences settings = ParentActivity.getSharedPreferences("com.omegaedi.magtek.MyPrefsFile", 0);
        switch (msg.what) {
            case MTSCRAEvent.OnDeviceConnectionStateChanged:
                MTConnectionState ConStatus = magtekReaderFunctions.HandleConnectionState((MTConnectionState) msg.obj);
                String color = "black";
                switch (ConStatus) {
                    case Connected:
                    	ParentActivity = cordova.getActivity();	
						String saved = settings.getString("ReaderAddress", "");						

						if(saved != m_deviceAddress){
							Log.i(TAG, "Trying to set them prefences");
							sendJavascript("Trying to set them prefences");
							SharedPreferences.Editor editor = settings.edit();
							editor.putString("ReaderAddress", m_deviceAddress);
							editor.commit();
						}
                        //startEDynamoEMVTransactionFlow();
                        break;
                    case Disconnected:
                        color = "red";
                        if (reconnect)
                            openMagtekDevice();
                        break;
                    case Disconnecting:
                        color = "red";
                        break;
                    default:

                        break;
                }
                //toast(magtekReaderFunctions.outMsg);
                OnDeviceStateChanged((MTConnectionState) msg.obj);
                break;
            case MTSCRAEvent.OnCardDataStateChanged:
                magtekReaderFunctions.MagtekOnCardDataStateChanged((MTCardDataState) msg.obj);
                Log.i(TAG, magtekReaderFunctions.outMsg);
                sendJavascript(magtekReaderFunctions.outMsg);
                break;
            case MTSCRAEvent.OnDataReceived:
            	// TEMPORARILY BYPASSING
                // if(magtekReaderFunctions.isEMVCard(mMTSCRA.getTrack1Masked(), mMTSCRA.getTrack2Masked()) && !fallbackEnabled){
                //     magtekReaderFunctions.ClearMagtekData();
                //     toast("Please Insert Card Using Chip Reader");

                //     startEDynamoEMVTransactionFlow();
                // } else {
                    magtekReaderFunctions.MagtekDataReceived(mMTSCRA);
                    CardEntryMethod = TTCardEntrySwipe;
                    //successful swipe, you can proceed with tran
                    //OnCardDataReceived((IMTCardData) msg.obj);
                    try{
                    	//sendJavascript("Here I am in Data Received");
                    	sendTransactionResult(TTCardReaderEDynamo);
                    	sendJavascript("Gonna clear'em!");
                    	Log.i(TAG, "Gonna clear'em!");
                    	mMTSCRA.clearBuffers();
            		}
            		catch(JSONException e){}
                //}

                break;
            case MTSCRAEvent.OnDeviceResponse:
                break;
            case MTEMVEvent.OnTransactionStatus:
                String tranStatus = TLVParser.getHexString((byte[]) msg.obj);
                switch(tranStatus.substring(0,2)){
                	case "08":
                		//toast("Card Removed");
                		Log.i(TAG, "Card Removed");
                		sendJavascript("Card Removed");
                    	//startEDynamoEMVTransactionFlow();
                    	break;
                	case "01":
                		Log.i(TAG, "Waiting for User to insert Card");
                		sendJavascript("Waiting for User to insert Card");
                		break;
                	case "02":
                		toast("Card Error");
                		Log.i(TAG, "Card Error");
                		sendJavascript("Card Error");
                		break;
            		case "03":
            			Log.i(TAG, "Transaction Progress change");
            			//sendJavascript("Transaction Progress change");
            			Log.i(TAG, "Current code: " + tranStatus.substring(4, 6));
            			//sendJavascript("Current code: " + tranStatus.substring(4, 6));
            			Log.i(TAG, tranStatus.substring(2, 4) + " seconds remaining");
            			//sendJavascript(tranStatus.substring(2, 4) + " seconds remaining");
            			break;
            		case "04":
            			Log.i(TAG, "Waiting for User Input...");
            			sendJavascript("Waiting for User Input...");
            			Log.i(TAG, tranStatus.substring(2, 4) + " seconds remaining");
            			//sendJavascript(tranStatus.substring(2, 4) + " seconds remaining");
            			break;
        			case "05":
        				toast("Transaction Timed Out");
        				Log.i(TAG, "Transaction Timed Out");
        				sendJavascript("Transaction Timed Out");

        				
        				sendError("Timed Out");
        				mMTSCRA.clearBuffers();
        				break;
    				case "06":
    					//toast("Transaction terminated");
    					Log.i(TAG, "Transaction terminated");
    					sendJavascript("Transaction terminated");
    					mMTSCRA.clearBuffers();
    					break;
    				default:
    					Log.i(TAG, "Sumfin else mate: " + tranStatus.substring(0,2));
    					sendJavascript("Sumfin else mate: " + tranStatus.substring(0,2));
    					break;
                }
                break;
            case MTEMVEvent.OnDisplayMessageRequest:
                String message = TLVParser.getTextString((byte[]) msg.obj, 0);
                Log.i(TAG, "display message request: " + message);
                sendJavascript("display message request: " + message);
                if(message.equals("PRESENT CARD")){
                    updateText("Waiting for card", "black");
                }
                break;
            case MTEMVEvent.OnUserSelectionRequest:
            	Log.i(TAG, "UserSelectionRequest happed");
            	sendJavascript("UserSelectionRequest happed");
                //OnUserSelectionRequest((byte[]) msg.obj);
                //need to try to auto-select and then we will display as needed
                handleUserSelection((byte[]) msg.obj);
                break;
            case MTEMVEvent.OnARQCReceived:
            	Log.i(TAG, "ARQReceived");
            	sendJavascript("ARQReceived");
                magtekReaderFunctions.buildOutEMVCardEventParams((byte[]) msg.obj, mMTSCRA);
                CardEntryMethod = TTCardEntryInsert;
                //successful insert you can proceed with tran
                break;
            case MTEMVEvent.OnTransactionResult:
            	// toast("Transaction Result Happened");
            	sendJavascript("OnTransactionResult Happened");
                byte[] data = (byte[]) msg.obj;
                if(data!=null) {
                    if (data.length > 0) {
                        int lenBatchData = data.length - 3;
                        if (lenBatchData > 0) {
                            byte[] batchData = new byte[lenBatchData];

                            System.arraycopy(data, 3, batchData, 0, lenBatchData);

                            List<HashMap<String, String>> parsedTLVList = TLVParser.parseEMVData(batchData, false, "");

                            String cidString = TLVParser.getTagValue(parsedTLVList, "DFDF1A");
                            byte[] responseTag = TLVParser.getByteArrayFromHexString(cidString);
                            
                        	
            /*
            0x00 = Approved
            0x01 = Declined
            0x02 = Error
            0x10 = Cancelled by Host
            0x1E = Manual Selection Cancelled by Host
            0x1F = Manual Selection Timeout
            0x21 = Waiting for Card Cancelled by Host
            0x22 = Waiting for Card Timeout
            0x23 = Cancelled by Card Swipe
            0xFF = Unknown
            */
            				//toast("Tag Result: " + responseTag[0]);
            				// If it's not an Error, Waiting for Card Timeout, Manual Selection Timeout, Cancelled by Card Swipe, or Declined
                            if (responseTag[0] != 0x02 && responseTag[0] != 0x22 && responseTag[0] != 0x1F && responseTag[0] != 0x23 && responseTag[0] != 0x01) {
                                if (emvAttemptsBeforeFallbackSwipe > 0) {
                                    emvAttemptsBeforeFallbackSwipe--;
                                }
                                String alertTitle = "Chip Comm Error";
                                String alertMessage;
                                if (emvAttemptsBeforeFallbackSwipe > 0) {
                                    alertMessage = "Remove card and try again\r\n" + emvAttemptsBeforeFallbackSwipe + " attempts remaining";
                                } else {
                                    fallbackEnabled = true;
                                    alertMessage = "Please swipe your current card or insert a different one";
                                }

                                HashMap<String, String> error = new HashMap<>();
                                error.put("TITLE", alertTitle);
                                error.put("MESSAGE", alertMessage);
                                Log.i(TAG, error.toString());
                                sendJavascript(error.toString());
                                toast(error.toString());
                            }

                         //    if (responseTag[0])
                            
                            try{
                            	sendTransactionResult(TTCardReaderEDynamo);
                        	}
                        	catch(JSONException e){}
                        }
                    }
                }

                magtekReaderFunctions.ClearMagtekData();
                break;

            case MTEMVEvent.OnEMVCommandResult:
                //log if you want
            	byte[] results = (byte[]) msg.obj;
            	Log.i(TAG, "EMVCommandResult: " + results[0]);
            	sendJavascript("EMVCommandResult: " + results[0]);
                break;

            case MTEMVEvent.OnDeviceExtendedResponse:
                String extendedMessage = (String) msg.obj;
                Log.i(TAG, "extended message request: " + extendedMessage);
                sendJavascript("extended message request: " + extendedMessage);
                break;
            default:
                break;
        }
        return true;
    }
}

	private void setState(MTConnectionState deviceState)
		{
			m_connectionState = deviceState;
		}

    protected void OnCardDataReceived(IMTCardData cardData)
    {
    	// toast("data received");
	    //clearDisplay();
			try{
    	//sendToDisplay("[Raw Data]");
    	//sendToDisplay(mMTSCRA.getResponseData());

    	//sendToDisplay("[Card Data]");
		if (cardData != null)
    		this.sendCardData();

    	// if (mStringLocalConfig.length() > 0) {
    	// 	setConfigurationLocal(mStringLocalConfig);
    	// 	mStringLocalConfig = "";
    	// }
    	//sendToDisplay("[TLV Payload]");
    	//sendToDisplay(cardData.getTLVPayload());
		}
			catch(JSONException e)
			{}
    }

	private void openMagtekDevice() {
        mMTSCRA.openDevice();
	}

	private ArrayList<String> getSelectionList(byte[] data)
	{
	    ArrayList<String> selectionList = new ArrayList<>();

	    if (data != null) {
	        int dataLen = data.length;

	        if (dataLen >= 2) {
	            int start = 2;

	            for (int i = 2; i < dataLen; i++) {
	                if (data[i] == 0x00) {
	                    int len = i - start;

	                    if (len >= 0) {
	                        selectionList.add(new String(data, start, len));
	                    }
	                    start = i + 1;
	                }
	            }
	        }
	    }

	    return selectionList;
	}

	private void handleUserSelection(byte[] aSelectionData){
	    if (aSelectionData != null) {
	        int dataLen = aSelectionData.length;

	        if (dataLen > 2) {
	            byte selectionType = aSelectionData[0];

	            ArrayList<String> selectionList = getSelectionList(aSelectionData);

	            String selectionTitle = selectionList.get(0);

	            selectionList.remove(0);

	            int nSelections = selectionList.size();

	            if (nSelections > 0) {
	                if (selectionType == MTEMVDeviceConstants.SELECTION_TYPE_LANGUAGE) { //auto select English
	                    for (int i = 0; i < nSelections; i++) {
	                        byte[] code = selectionList.get(i).getBytes();
	                        EMVLanguage language = EMVLanguage.GetLanguage(code);


	                        if (language != null) {
	                            if(language == EMVLanguage.LANGUAGE_ENGLISH){ //auto-select english
	                                mMTSCRA.setUserSelectionResult(MTEMVDeviceConstants.SELECTION_STATUS_COMPLETED, (byte) i);
	                                return;
	                            }
	                            selectionList.set(i, language.getName());
	                        }
	                    }
	                }

	                String[] selectionArray = selectionList.toArray(new String[selectionList.size()]);

	                if(selectionList.size() == 1){
	                    mMTSCRA.setUserSelectionResult(MTEMVDeviceConstants.SELECTION_STATUS_COMPLETED, (byte) 0);
	                    return;
	                }

	                UCRL.UserSelectionRequired(selectionTitle, selectionArray);
	            }
	        }
	    }
	}


}
