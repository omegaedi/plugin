package com.omegaedi.cordova.plugins.magtek;

import com.magtek.mobile.android.mtlib.MTCardDataState;
import com.magtek.mobile.android.mtlib.MTConnectionState;
import com.magtek.mobile.android.mtlib.MTConnectionType;
import com.magtek.mobile.android.mtlib.MTSCRA;

import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by drewl on 2/14/2018.
 */

public class MagtekReader {

    public String outMsg;

    public String mtKSN;
    public String mtDeviceSerial;

    //swipe readers
    public String mtSessionID;
    public String mtTrack1;
    public String mtTrack2;
    public String mtMPData;
    public String mtMPStatus;
    public String mtMaskedPAN;
    public String mtCardName;

    //emv readers
    public String mtEMVDataBlock;
    public String mtEncryptedBlock;
    public String mtDeviceModel;
    public String mtEMVEncoding;


    public MTConnectionState HandleConnectionState(MTConnectionState conState) {
        switch (conState) {
            case Connected:
                outMsg = "Waiting for Card Swipe";
                break;
            case Connecting:
                outMsg = "Connecting to reader";
                break;
            case Disconnected:
                outMsg = "Disconnected from reader";
                break;
            case Disconnecting:
                outMsg = "Disconnecting from reader";
                break;
            case Error:
                outMsg = "Error communicating with reader";
                break;
        }
        return conState;
    }

    public void MagtekOnCardDataStateChanged(MTCardDataState cardDataSt){
        if (cardDataSt != null)  {
            switch(cardDataSt){
                case DataNotReady:
                    outMsg = "Card Swiped...";
                    break;
                case DataReady:
                    outMsg = "Processing Swipe...";
                    break;
                case DataError:
                    outMsg = "Card swipe error...";
                    break;
                default:
                    outMsg = "";
            }
        }
    }

    public void ClearMagtekData(){
        mtKSN = "";
        mtDeviceSerial = "";

        //swipe readers
        mtSessionID = "";
        mtTrack1 = "";
        mtTrack2 = "";
        mtMPData = "";
        mtMPStatus = "";
        mtMaskedPAN = "";
        mtCardName = "";

        //emv readers
        mtEMVDataBlock = "";
        mtEncryptedBlock = "";
        mtDeviceModel = "";
        mtEMVEncoding = "";
    }

    public void MagtekDataReceived(MTSCRA mt){
        mtKSN = mt.getKSN();
        mtSessionID = mt.getSessionID();
        mtTrack1 = mt.getTrack1();
        mtTrack2 = mt.getTrack2();
        mtDeviceSerial  = mt.getDeviceSerial();
        mtMPData = mt.getMagnePrint();
        mtMPStatus = mt.getMagnePrintStatus();

        int len = mt.getCardPANLength();
        if (len > 0) {
            String maskedPAN = "";
            for (int i=0; i<len-4; i++)
                maskedPAN += "X";
            maskedPAN += mt.getCardLast4();
            mtMaskedPAN = maskedPAN;

            mtCardName = mt.getCardName();

            outMsg = "Read Successful";
        } else {
            outMsg = "Error: please swipe card again";
        }
    }

    private byte[] buildAcquirerResponseFormat(byte[] macKSN, byte[] macEncryptionType, byte[] deviceSN)
    {
        byte[] response;

        int lenMACKSN = 0;
        int lenMACEncryptionType = 0;
        int lenSN = 0;

        if (macKSN != null)
        {
            lenMACKSN = macKSN.length;
        }

        if (macEncryptionType != null)
        {
            lenMACEncryptionType = macEncryptionType.length;
        }

        if (deviceSN != null)
        {
            lenSN = deviceSN.length;
        }

        byte[] macKSNTag = new byte[] { (byte)0xDF, (byte)0xDF, 0x54, (byte)lenMACKSN };
        byte[] macEncryptionTypeTag = new byte[] { (byte)0xDF, (byte)0xDF, 0x55, (byte)lenMACEncryptionType };
        byte[] snTag = new byte[] { (byte)0xDF, (byte)0xDF, 0x25, (byte)lenSN };
        byte[] container = new byte[] { (byte)0xFA, 0x06, 0x70, 0x04 };
        byte[] approvedARC = new byte[] { (byte)0x8A, 0x02, 0x5A, 0x33 };

        int lenTLV = 4 + macKSNTag.length + lenMACKSN + macEncryptionTypeTag.length + lenMACEncryptionType + snTag.length + lenSN + container.length + approvedARC.length;

        int lenPadding = 0;

        if ((lenTLV % 8) > 0)
        {
            lenPadding = (8 - lenTLV % 8);
        }

        int lenData = lenTLV + lenPadding + 4;

        response = new byte[lenData];

        int i = 0;
        response[i++] = (byte)(((lenData - 2) >> 8) & 0xFF);
        response[i++] = (byte)((lenData - 2) & 0xFF);
        response[i++] = (byte)0xF9;
        response[i++] = (byte)(lenTLV - 4);
        System.arraycopy(macKSNTag, 0, response, i, macKSNTag.length);
        i += macKSNTag.length;
        if(macKSN != null) {
            System.arraycopy(macKSN, 0, response, i, macKSN.length);
            i += macKSN.length;
        }
        System.arraycopy(macEncryptionTypeTag, 0, response, i, macEncryptionTypeTag.length);
        i += macEncryptionTypeTag.length;
        if(macEncryptionType != null) {
            System.arraycopy(macEncryptionType, 0, response, i, macEncryptionType.length);
            i += macEncryptionType.length;
        }
        System.arraycopy(snTag, 0, response, i, snTag.length);
        i += snTag.length;
        if(deviceSN != null) {
            System.arraycopy(deviceSN, 0, response, i, deviceSN.length);
            i += deviceSN.length;
        }
        System.arraycopy(container, 0, response, i, container.length);
        i += container.length;

        System.arraycopy(approvedARC, 0, response, i, approvedARC.length);

        return response;
    }

    public void buildOutEMVCardEventParams(byte[] data, MTSCRA mMTSCRA){
        List<HashMap<String, String>> parsedTLVList = TLVParser.parseEMVData(data, true, "");

        if (parsedTLVList != null)
        {
            String deviceKSNString = TLVParser.getTagValue(parsedTLVList, "DFDF56");

            String encryptedBlock = TLVParser.getTagValue(parsedTLVList, "DFDF59");

            String deviceSNString = TLVParser.getTextString(TLVParser.getByteArrayFromHexString(TLVParser.getTagValue(parsedTLVList, "DFDF25")), 0);

            String maskedPAN = TLVParser.getTextString(TLVParser.getByteArrayFromHexString(TLVParser.getTagValue(parsedTLVList, "DFDF4D")), 1);
            if(maskedPAN != null){
                    String[] split = maskedPAN.split("=");
                maskedPAN = split[0];
                maskedPAN = maskedPAN.substring(0,6) + "XXXXXX" + maskedPAN.substring(12);
                mtMaskedPAN=maskedPAN;
            }

            String EMVDataBlock ="";

            ListIterator<HashMap<String, String>> it = parsedTLVList.listIterator();

            while (it.hasNext())
            {
                HashMap<String, String> map = it.next();

                if ((!map.get("tag").contains("DF")) && (!map.get("tag").contains("F8")) && (!map.get("tag").contains("F9")) && (!map.get("tag").contains("FA")) && (!map.get("tag").contains("F7")))
                {
                    if(!map.get("tag").startsWith("F") && !map.get("tag").startsWith("2") && map.get("tag").startsWith("5F")) {
                        if (map.get("tag").contains("5F30") || map.get("tag").contains("5F20")) {
                            int length;
                            try{
                                length = Integer.parseInt(map.get("len"));
                            } catch(Exception e){
                                length = 0;
                            }
                            EMVDataBlock += map.get("tag") + String.format("%02X",length) + map.get("value");
                        }
                    }
                }
            }

            mtEncryptedBlock = encryptedBlock;
            mtKSN = deviceKSNString;
            mtDeviceSerial = deviceSNString;
            mtDeviceModel = "eDynamo";

            mtEMVEncoding = "BLOCK";
            mtEMVDataBlock = EMVDataBlock;

            outMsg = "Read Successful";

            String macKSNString = TLVParser.getTagValue(parsedTLVList, "DFDF54");
            byte[] macKSN = TLVParser.getByteArrayFromHexString(macKSNString);

            String macEncryptionTypeString = TLVParser.getTagValue(parsedTLVList, "DFDF55");
            byte[] macEncryptionType = TLVParser.getByteArrayFromHexString(macEncryptionTypeString);

            byte[] deviceSN = TLVParser.getByteArrayFromHexString(deviceSNString);

            //send back a second gen ac of Z3 to fool the collis tool and FD because they are stupid and are making the device appear to be complete emv even though it only supposed to be quickChip.
            //0x5A   0x33   //Z3
            byte[] dummyPacket=buildAcquirerResponseFormat(macKSN, macEncryptionType, deviceSN);

            mMTSCRA.setAcquirerResponse(dummyPacket);

        }
    }

    public boolean isEMVCard(String maskedTrack1, String maskedTrack2) {
        String trackOneStringToDecideEMV = null;
        String trackTwoStringToDecideEMV = null;
        String subStringFromTrackOneToDecideEMV = null;
        String subStringFromTrackTwoToDecideEMV = null;

        try {
            try {
                if (maskedTrack1 != null && !(maskedTrack1.equals(""))) {
                    String[] trackOneMaskedData = maskedTrack1.split("\\^");
                    if (trackOneMaskedData.length > 2) {
                        trackOneStringToDecideEMV = trackOneMaskedData[2];
                    }
                }

                if (trackOneStringToDecideEMV != null && trackOneStringToDecideEMV.length() > 4) {
                    subStringFromTrackOneToDecideEMV = trackOneStringToDecideEMV.substring(4, 5);
                }
            } catch (Exception e) {
                subStringFromTrackOneToDecideEMV = null;
            }

            try {
                if (maskedTrack2 != null && !(maskedTrack2.equals(""))) {
                    String[] trackTwoMaskedData = maskedTrack2.split("=");
                    if (trackTwoMaskedData.length > 1) {
                        trackTwoStringToDecideEMV = trackTwoMaskedData[1];
                    }
                }

                if (trackTwoStringToDecideEMV != null && trackTwoStringToDecideEMV.length() > 4) {
                    subStringFromTrackTwoToDecideEMV = trackTwoStringToDecideEMV.substring(4, 5); //check position 5
                }
            } catch (Exception e) {
                subStringFromTrackTwoToDecideEMV = null;
            }
        }
        catch(Exception e) { //card track data can do weird things so this will get us through in case of said weird things
            subStringFromTrackOneToDecideEMV = null;
            subStringFromTrackTwoToDecideEMV = null;
        }

        if ((subStringFromTrackOneToDecideEMV != null && (subStringFromTrackOneToDecideEMV.equals("2") || subStringFromTrackOneToDecideEMV.equals("6"))) ||
                (subStringFromTrackTwoToDecideEMV != null && (subStringFromTrackTwoToDecideEMV.equals("2") || subStringFromTrackTwoToDecideEMV.equals("6")))){
            return true;
        }
        return false;
    }
}