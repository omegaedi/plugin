package com.omegaedi.cordova.plugins.magtek;

import java.util.HashMap;

public interface UniversalCardReaderListener {
    void updateText(HashMap<String, String> newText);
    void StartReaderResult(HashMap<String, String> result);
    void CardDataCaptured(HashMap<String, String> captureResult);
    void CardInsertFailure(HashMap<String, String> error);
    void UserSelectionRequired(String selectionTitle, String[] selectionArray);
    void PleaseInsertCard(String title, String message);
}