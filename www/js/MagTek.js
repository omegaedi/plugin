'use strict';

    var MagTek = ( typeof MagTek === 'undefined' ? {} : MagTek );
    var cordova = window.cordova || window.Cordova,
        fail = function(error) {
            console.log('Error running your request: ' + error); //TODO: Rollbar this boy like a doobie
        };


    MagTek.isDeviceConnected = function(callback, error) {
        var success = function(connected) {
            callback(connected);
        };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'isDeviceConnected', []);
    };

    MagTek.chooseDevice = function(callback, error, arg0) {
      var success = function(choice) {
        callback(choice);
      };
      var fail_handler = error || fail;

      cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'chooseDevice', [arg0]);
    };

    MagTek.defaultDevice = function(callback, error) {
      var success = function(choice) {
        callback(choice);
      };
      var fail_handler = error || fail;

      cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'defaultDevice', []);
    };

    MagTek.isDeviceOpened = function(callback, error) {
        var success = function(opened) { callback(opened); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'isDeviceOpened', []);
    };

    MagTek.openDevice = function(callback, error) {
        var success = function(status) { callback(status); }
        var fail_handler = error || fail;;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'openDevice', []);
    };
    MagTek.closeDevice = function(callback, error) {
        var success = function(status) { callback(status); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'closeDevice', []);
    };
    MagTek.clearCardData = function(callback, error) {
        var success = function(data_cleared) { callback(data_cleared); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'clearCardData', []);
    };
    MagTek.getBatteryLevel = function(callback, error) {
        var success = function(battery) { callback(battery); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getBatteryLevel', []);
    };
    MagTek.setCardData = function(callback, error) {
        var success = function(data_set) { callback(data_set); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'setCardData', []);
    };
    MagTek.getTrackDecodeStatus = function(callback, error) {
        var success = function(decode_status) { callback(decode_status); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getTrackDecodeStatus', []);
    };
    MagTek.getTrack1 = function(callback, error) {
        var success = function(track) { callback(track); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getTrack1', []);
    };
    MagTek.getTrack2 = function(callback, error) {
        var success = function(track) { callback(track); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getTrack2', []);
    };
    MagTek.getTrack3 = function(callback, error) {
        var success = function(track) { callback(track); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getTrack3', []);
    };
    MagTek.getTrack1Masked = function(callback, error) {
        var success = function(track) { callback(track); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getTrack1Masked', []);
    };
    MagTek.getTrack2Masked = function(callback, error) {
        var success = function(track) { callback(track); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getTrack2Masked', []);
    };
    MagTek.getTrack3Masked = function(callback, error) {
        var success = function(track) { callback(track); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getTrack3Masked', []);
    };
    MagTek.getMagnePrintStatus = function(callback, error) {
        var success = function(status) { callback(status); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getMagnePrintStatus', []);
    };
    MagTek.getMagnePrint = function(callback, error) {
        var success = function(magne_print) { callback(magne_print); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getMagnePrint', []);
    };
    MagTek.getDeviceSerial = function(callback, error) {
        var success = function(device_serial) { callback(device_serial); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getDeviceSerial', []);
    };
    MagTek.getSessionID = function(callback, error) {
        var success = function(session_id) { callback(session_id); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getSessionID', []);
    };


    //..... A few methods skipped


    MagTek.getPaired = function(callback, error) {
      var success = function(paired) { callback(paired); };
      var fail_handler = error || fail;

      cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getPaired', []);
    };

    MagTek.getSavedDevice = function(callback, error) {
        var success = function(device) { callback(device); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getSavedDevice', []);
    }

    MagTek.setSavedDevice = function(callback, error, arg0){
        var success = function(address) { callback(address);};
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'setSavedDevice', [arg0])
    }

    MagTek.setAmount = function(callback, error, arg0){
        var success = function(status) { callback(status); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'setAmount', [arg0])
    }

    MagTek.setDeviceProtocolString = function(callback, protocol_string) {
        var success = function(status) { callback(status); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'setDeviceProtocolString', [protocol_string]);
    };
    MagTek.listenForEvents = function(callback, error) {
        var success = function(status) { callback(status); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'listenForEvents', []);
    };
    MagTek.listenForCardEvents = function(callback, error) {
        var success = function(status) { callback(status); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'listenForCardEvents', []);
    };

    MagTek.getCardName = function(callback, error) {
        var success = function(card_name) { callback(card_name); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getCardName', []);
    };
    MagTek.getCardIIN = function(callback, error) {
        var success = function(card_iin) { callback(card_iin); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getCardIIN', []);
    };
    MagTek.getCardLast4 = function(callback, error) {
        var success = function(card_last4) { callback(card_last4); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getCardLast4', []);
    };
    MagTek.getCardExpDate = function(callback, error) {
        var success = function(card_exp_date) { callback(card_exp_date); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getCardExpDate', []);
    };
    MagTek.getCardServiceCode = function(callback, error) {
        var success = function(card_svc) { callback(card_svc); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getCardServiceCode', []);
    };
    MagTek.getCardStatus = function(callback, error) {
        var success = function(card_status) { callback(card_status); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getCardStatus', []);
    };


    //..... A few methods skipped


    MagTek.getAddress = function(callback, error) {
      var success = function(dev_address) { callback(dev_address); };
      var fail_handler = error || fail;

      cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'getDevAddress', []);
    };

    MagTek.setDeviceType = function(callback, error) {
        var success = function(status) { callback(status); };
        var fail_handler = error || fail;

        cordova.exec(success, fail_handler, 'com.omegaedi.magtek', 'setDeviceType', []);
    };


    module.exports = MagTek;
